import gzip
from csv import writer, reader
from functools import partial
from json import loads
from os import walk, makedirs, listdir
from os.path import join, exists, dirname, basename, isdir, isfile
from pathlib import Path

from returns.io import impure_safe
from returns.pipeline import flow
from returns.pointfree import bind
from returns.result import safe, Failure, Result, Success


class FileProcessingException(Exception):
    def __init__(self, message):
        self.message = message

    def __str__(self):
        print(self.message)


def process(root_dir: str, output_dir: str):
    func = partial(_process_dirs, root_dir=root_dir)
    flow(
        output_dir,
        create_processed_dir,
        bind(func)
    )


def _process_dirs(output_dir: str, root_dir: str):
    for dir_path, dir_names, file_names in walk(root_dir):
        if file_names:
            print(f"Processing files in directory {dir_path}")
            _process_files(dir_path, file_names, output_dir)
        for dn in dir_names:
            _process_dirs(output_dir, dn)


def _process_files(dir_path: str, files: list[str], output_dir: str):
    if not files:
        print(f"No files to process in directory {dir_path}")
        return
    first_file_path = join(dir_path, files[0])
    absolute_file_paths = [join(dir_path, file) for file in files]
    consolidate_func = partial(consolidate_files, output_dir=output_dir, files=absolute_file_paths)
    return flow(
        first_file_path,
        get_headers,
        bind(consolidate_func)
    )


@impure_safe
def consolidate_files(headers: list[str], output_dir: str, files: list[str]) -> Result[str, FileProcessingException]:
    if not files:
        return Failure(FileProcessingException("No files were provided"))
    output_file_name = f"{basename(dirname(files[0]))}"
    output_file_type = "gz"
    output_file_path = join(output_dir, f"{output_file_name}.{output_file_type}")
    with gzip.open(output_file_path, mode="wt") as output_file:
        w = writer(output_file)
        w.writerow(headers)
        for file in files:
            with open(file, mode="r") as read_file:
                file_type = Path(file).suffix
                if file_type == ".csv":
                    # process csv
                    csv_reader = reader(read_file, delimiter=",")
                    # skip csv header since we already have the header
                    next(csv_reader)
                    for row in csv_reader:
                        w.writerow(row)
                else:
                    # process json
                    for line in read_file.readlines():
                        json_values = loads(line).values()
                        w.writerow(json_values)
    return Success(output_file_path)


@impure_safe
def create_processed_dir(dir_path: str) -> str:
    if not exists(dir_path):
        makedirs(dir_path)
    return dir_path


@safe
def get_headers(file_path: str):
    file_type = Path(file_path).suffix
    with open(file_path) as file:
        first_line = file.readline()
        if file_type == ".csv":
            return _get_csv_headers(first_line)
        else:
            return _get_json_headers(first_line)


def _get_json_headers(json: str) -> list[str]:
    json_dict = loads(json)
    return json_dict.keys()


def _get_csv_headers(header_row: str) -> list[str]:
    return header_row.strip().split(",")


if __name__ == "__main__":
    # Input your raw_data path and desired output file paths like below.
    process("/home/alan/dev/raw_data/", "/home/alan/dev/processed_data")
